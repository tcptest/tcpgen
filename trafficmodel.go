package main

import(
  "flag"
  "github.com/jimlawless/cfg"
  "fmt"
  "strconv"
  "os"
)

type trafficModel struct{
  pages,objects,objsize,intersession,interpage,interobj *float64
  pagesd,objectsd,objsized,intersessiond,interpaged,interobjd *string
}

func (tm *trafficModel)initFromFlags(){
  var trflag = flag.NewFlagSet("trflag", flag.ContinueOnError)
  tm.pages = trflag.Float64("p",60, "Average number of pages per session")
  tm.pagesd = trflag.String("pd","exp", "Average number of pages per session distribution ")
  tm.objects = trflag.Float64("o",3, "Average number of objects per pages ")
  tm.objectsd = trflag.String("od","exp", "Average number of objects per pages distribution ")
  tm.objsize = trflag.Float64("os",1.7, "Average object size (in pkt)")
  tm.objsized = trflag.String("osd","zipf", "Average object size distribution (in pkt)")
  tm.intersession = trflag.Float64("is",8000, "Average intersession time (ms)")
  tm.intersessiond = trflag.String("isd","exp", "Average intersession time distribution (ms)")
  tm.interpage = trflag.Float64("ip",5000, "Average interpage time (ms)")
  tm.interpaged = trflag.String("ipd","exp", "Average interpage time distribution (ms)")
  tm.interobj = trflag.Float64("io",500, "Average interobject time (ms)")
  tm.interobjd = trflag.String("iod","exp", "Average interobject time distribution (ms)")
  trflag.Parse(os.Args)
}

func (tm *trafficModel)initFromConfFile(conffile string){
  if conffile != ""{
    confMap := make(map[string]string)
    err := cfg.Load(conffile, confMap)
    if err != nil {
      fmt.Println("error:",err)
    }
    fmt.Printf("%v\n", confMap)
    if val, ok := confMap["pages"]; ok {
        *tm.pages,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["pagesd"]; ok {
        *tm.pagesd=val
    }
    if val, ok := confMap["objects"]; ok {
      *tm.objects,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["objectsd"]; ok {
      *tm.objectsd=val
    }
    if val, ok := confMap["objsize"]; ok {
      *tm.objsize,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["objsized"]; ok {
      *tm.objsized=val
    }
    if val, ok := confMap["intersession"]; ok {
      *tm.intersession,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["intersessiond"]; ok {
      *tm.intersessiond=val
    }
    if val, ok := confMap["interpage"]; ok {
      *tm.interpage,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["interpaged"]; ok {
      *tm.interpaged=val
    }
    if val, ok := confMap["interobj"]; ok {
      *tm.interobj,err=strconv.ParseFloat(val,64)
    }
    if val, ok := confMap["interobjd"]; ok {
      *tm.interobjd=val
    }
  }
  return
}

func (tm *trafficModel)init(conffile string){
  tm.initFromFlags()
  tm.initFromConfFile(conffile)
  return
}
