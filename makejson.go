package main

import(
  "encoding/json"
  "os"
  "io/ioutil"
  "fmt"
)

type Connections struct{
  Reqclients []ReqClient
}

type Request struct{
  Starttime uint32 `json:"starttime"`
  Toreceive uint64 `json:"toreceive"`
  Tosend uint64 `json:"tosend"`
}

type ReqClient struct{
  // Arrays of all requests in a connection
  Requests []Request `json:"requests"`
  Npages int `json:"-"`
  // Time to start in ms
  Starttime uint32 `json:"starttime"`
}

func makeJson(cx Connections,file string){
  b, err := json.Marshal(cx)
  if err != nil {
    fmt.Println("error:", err)
  }

  if file != "" {
    err := ioutil.WriteFile(file,b,0644)
    if err != nil {
      fmt.Println("error:",err)
    }
  } else{
    os.Stdout.Write(b)
  }
  return
}

