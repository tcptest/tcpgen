package main

import(
  "gitlab.com/u/Tbraud/random"
  "flag"
  "fmt"
  "os"
)

func checkCorresp(distrib string)int{
  corresp := make(map[string]int) 
  corresp["exp"]=random.NEGEXP
  corresp["uni"]=random.UNIFORM
  corresp["zipf"]=random.ZIPF

  if _, ok := corresp[distrib]; ok {
    return corresp[distrib]
  }
  os.Exit(1)
  return -1
}


func main()  {
  var time = flag.Int64("d", 100000, "Duration (in ms)") 
  var seed1 = flag.Int("s", 12345, "Seed") 
  var file = flag.String("f","", "Filename")
  var conffile = flag.String("cf","", "Filename")
  flag.Parse()

  var tm trafficModel
  tm.init(*conffile)


  var seed=int32(*seed1)

  var tmptime,tmptimereq int64;

  var objectnum, tosend,toreceive,nextobject,nextpage,nextsession float64

  var cx Connections;
  for tmptime<*time{
    var rq ReqClient
    tmptimereq=0
    rq.Starttime=uint32(tmptime)
    //Session
    pagenum,seed,_:=random.GetValue(seed,checkCorresp(*tm.pagesd),*tm.pages)
    rq.Npages+=int(pagenum)
    for i:=0;i<int(pagenum);i++{
      objectnum,seed,_=random.GetValue(seed,checkCorresp(*tm.objectsd),*tm.objects)
      for j:=0;j<int(objectnum);j++{
        var r Request;
        tosend,seed,_=random.GetValue(seed,checkCorresp(*tm.objsized),*tm.objsize)
        toreceive,seed,_=random.GetValue(seed,checkCorresp(*tm.objsized),*tm.objsize)
        nextobject,seed,_=random.GetValue(seed,checkCorresp(*tm.interobjd),*tm.interobj)

        r.Tosend=uint64(tosend*100)
        r.Toreceive=uint64(toreceive*1500)
        r.Starttime=uint32(tmptime)
        rq.Requests=append(rq.Requests,r)
        tmptimereq+=int64(nextobject)
      }
      nextpage,seed,_=random.GetValue(seed,checkCorresp(*tm.interpaged),*tm.interpage)
      tmptimereq+=int64(nextpage)
    }
    nextsession,seed,_=random.GetValue(seed,checkCorresp(*tm.intersessiond),*tm.intersession)
    tmptime+=int64(nextsession)
    cx.Reqclients=append(cx.Reqclients,rq)
  }

  makeJson(cx,*file)

  fmt.Println("Generated:")
  fmt.Println(len(cx.Reqclients),"connexions")
  nreq:=0
  npages:=0
  for _,rq:= range(cx.Reqclients){
    nreq+=len(rq.Requests)
    npages+=rq.Npages
  }
  fmt.Println(npages,"pages")
  fmt.Println(nreq,"requests")


}
